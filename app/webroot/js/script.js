window.mapInit = function() {
	var map;
	if( $('#map').length ) {

		var myLatLng = {lat: 48.311634, lng: 18.083817};
		var mapOptions = {
			center: myLatLng,
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			zoom: 15,
			draggable: false,
			scrollwheel: false
		};


		map = new google.maps.Map(document.getElementById('map'), mapOptions);
	
		var marker = new google.maps.Marker({
			position: myLatLng,
			map: map
		});
	}
}

$(window).load(function() {

	$('body').removeClass('not-visible');

	var bxSliderOptions = {
		pager: false
	};

	$('.bxslider').bxSlider(bxSliderOptions);	
	
});

function submitContactForm() {

	$('form[name="contact-form"] input.error').removeClass('error');
	$('.result-message').fadeOut();

	var data = {
		"name":		document.forms['contact-form'].name.value,
		"email":	document.forms['contact-form'].email.value,
		"phone":	document.forms['contact-form'].phone.value,
		"message":	document.forms['contact-form'].message.value,
		// "captcha":	document.getElementById('g-recaptcha-response').value
	};

	$.ajax({
		dataType: 'json',
		type: "POST",
		url: "/process-contact-form",
		data: data
	})
	.done(function( result ) {

		if( result.result == 1 ) {
			$('#error-message').fadeOut();
			$('#success-message').text(result.message).fadeIn();
			$('form[name="contact-form"] input').not('input[type="submit"]').val('');
			$('form[name="contact-form"] textarea').val('');
		}
		else {
			if( result.data ) {
				result.data.forEach(function(field) {
					$('#contact-form').find('input[name="'+field+'"], textarea[name="'+field+'"]').addClass('error');
				});
			}
			$('#error-message').text(result.message).fadeIn();
		}

		console.error( result );

		// window.grecaptcha.reset();
	});
}
