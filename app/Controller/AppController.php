<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

	/**
	 * Metoda na inicializaciu emailu pri posielani. Samotna funkcia mail neposle.
	 * 
	 * @param mixed $to 1 a viac emailovych adries
	 * @param string $subject predmet
	 * @param string $template nazov templatu, ktory sa pouzije
	 * @param array $data data, ktore sa setnu do templatu
	 * @param string $layout layout, ktory sa pouzije
	 * @param mixed $from emailova adresa odosielatela
	 * @param string $type typ ako bude poslany email moznosti - text|html|both
	 * @return vrati objekt email
	 * @author martin.malych
	 */
	protected function initEmail($to = 'matus.wewo.bielik@gmail.com', $subject, $template, $data = array(), $layout = 'default', $from = null, $type = 'html', $attachements = null) {

		if( empty($from) ) {
			$from = array( 'evina@evina.sk' => 'Evina - Reklama');
		}			

		App::uses('CakeEmail', 'Network/Email');
		
		$email = new CakeEmail('default');

		$email->from($from);
		$email->to($to);
		$email->bcc('mbielik@mccann.sk');
		$email->subject($subject);
		$email->emailFormat($type);
		$email->template($template, $layout);

		if (!empty($attachements)) {
			$email->attachments($attachements);
		}

		if (!empty($data)) {
			$email->viewVars($data);
		}

		return $email;
	}
}
