<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Folder', 'Utility');
App::uses('File', 'Utility');
App::uses('AppController', 'Controller');

class PagesController extends AppController {

	public $uses = array();

	private function display($page, $title_for_layout, $folder = null, $data = null) {

		$carousel_images_folder = new Folder(WWW_ROOT.'img'.DS.'carousel'.DS.$folder);
		// $files = $dir->find('.*\.jpg');

		$carousel_data = array(
			'folder' => $folder,
			'count' => sizeof( $carousel_images_folder->find('.*\.jpg') )
		);

		$this->set(compact('page', 'title_for_layout', 'carousel_data', 'data'));

		try {
			$this->render($page);
		}
		catch (MissingViewException $e) {
			if (Configure::read('debug')) {
				throw $e;
			}
			return $this->redirect('/');
		}
	}

	public function home() {
		$page = 'home';
		$title_for_layout = 'Úvod';
		$this->display($page, $title_for_layout);
	}

	public function reklamne_predmety() {
		$page = 'reklamne_predmety';
		$title_for_layout = 'Reklamné predmety';

		$this->display($page, $title_for_layout, 'predmety');
	}

	public function reklamny_textil() {
		$page = 'reklamny_textil';
		$title_for_layout = 'Reklamný textil';

		$this->display($page, $title_for_layout, 'textil');
	}

	public function kalendare() {
		$page = 'kalendare';
		$title_for_layout = 'Kalendáre';

		$this->display($page, $title_for_layout, 'kalendare');
	}

	public function samolepiace_folie() {
		$page = 'samolepiace_folie';
		$title_for_layout = 'Samolepiace fólie';

		$this->display($page, $title_for_layout, 'folie');
	}

	public function firemne_tlacoviny() {
		$page = 'firemne_tlacoviny';
		$title_for_layout = 'Firemné tlačoviny';

		$this->display($page, $title_for_layout, 'tlacoviny');
	}

	public function reklamne_kadeco() {
		$page = 'reklamne_kadeco';
		$title_for_layout = 'Reklamné kadečo';

		$this->display($page, $title_for_layout, 'reklamne-kadeco');
	}

	public function graficke_navrhy() {
		$page = 'graficke_navrhy';
		$title_for_layout = 'Grafické návrhy';

		$this->display($page, $title_for_layout, 'graficke-navrhy');
	}

	public function referencie() {
		App::uses('Folder', 'Utility');
		App::uses('File', 'Utility');

		$logos_dir_path = WWW_ROOT.'/img/logos';

		$logos_dir = new Folder($logos_dir_path);
		$logos = $logos_dir->find('.*\.svg');

		$page = 'referencie';
		$title_for_layout = 'Referencie';

		$this->display($page, $title_for_layout, 'referencie', array('logos' => $logos));
	}

	public function kontakt() {
		$page = 'kontakt';
		$title_for_layout = 'Kontakt';

		$this->display($page, $title_for_layout, 'kontakt');
	}

	public function process_contact_form() {
		$this->autoRender = false;

		if( !$this->request->is('post') || !$this->request->is('ajax') ) {
			exit;
		}

		$validation_result = $this->validate_contact_form_data($this->request->data);


		if ( $validation_result !== true ) {
			$result = array(
				'result' => 0,
				'data' => $validation_result,
				'message' => 'Doplňte chýbajúce údaje'
			);

			exit( json_encode( $result ) );
		}

		$email = $this->initEmail( null, 'Správa z kontaktného formulára', 'contact_email', $this->request->data);

		if( $email->send( ) ) {
			$result['result']	= 1;
			$result['message']	= 'Email bol úspešne odoslaný';
		}
		else {
			$result['result']	= 0;
			$result['message']	= 'Email sa nepodarilo odoslať';
		}

		exit( json_encode($result) );
	}

	private function validate_contact_form_data($data) {
		$result = array(
			'email'		=> filter_var($data['email'], FILTER_VALIDATE_EMAIL),
			'name'		=> !empty($data['name']),
			'message'	=> !empty($data['message'])
		);

		return ( !$result['email'] || !$result['name'] || !$result['message'] ) ? $result : true;

	}
}
