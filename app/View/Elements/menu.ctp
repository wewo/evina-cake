<?php 

$menu_links = array(
	'/' => 'Úvod',
	'/reklamne-predmety'	=> 'Predmety',
	'/reklamny-textil'		=> 'Textil',
	'/kalendare'			=> 'Kalendáre',
	'/samolepiace-folie'	=> 'Fólie',
	'/firemne-tlacoviny'	=> 'Tlačoviny',
	'/reklamne-kadeco'		=> 'Kadečo',
	'/graficke-navrhy'		=> 'Návrhy',
	'/referencie'			=> 'Referencie',
	'/kontakt'				=> 'Kontakt'
);

$page = ($page == 'home') ? '' : $page;

?>

<nav class="navbar navbar-default" role="navigation">
	<div class="container">

		<div class="navbar-header">
			<button class="navbar-toggle collapsed" data-target="#navbar" data-toggle="collapse" type="button">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand visible-xs" href="/"></a>
		</div>

		<div class="collapse navbar-collapse" id="navbar">
			<a class="navbar-brand hidden-xs" href="/"></a>
			<ul class="nav navbar-nav">
				<?php foreach ($menu_links as $short_name => $full_name): ?>

				<?php
				$link_url = str_replace('-', '_', ltrim($short_name, '/'));
				$class = ($page == $link_url) ? 'active' : '';
				?>

				<li>
					<a href="<?= $short_name; ?>" class="<?= $class; ?>"><?= $full_name; ?></a>
				</li>
				<?php endforeach; ?>
			</ul>
		</div>
	</div>
</nav>