<footer>
	<div class="container">
		<address>
			<span class="green">EVINA - REKLAMA, s.r.o.</span><br>
			Piaristická 2, Nitra<br>
			037 / 654 23 11<br>
			<a href="mailto:evina@evina.sk">evina@evina.sk</a>
		</address>

		<ul class="footer-links">
			<li>
				<a href="/">Úvod</a>
			</li>
			<li>
				<a href="/reklamne-predmety">Predmety</a>
			</li>
			<li>
				<a href="/reklamny-textil">Textil</a>
			</li>
			<li>
				<a href="/kalendare">Kalendáre</a>
			</li>
			<li>
				<a href="/samolepiace-folie">Fólie</a>
			</li>
			<li>
				<a href="/firemne-tlacoviny">Tlačoviny</a>
			</li>
			<li>
				<a href="/reklamne-kadeco">Kadečo</a>
			</li>
			<li>
				<a href="/graficke-navrhy">Návrhy</a>
			</li>
			<li>
				<a href="/referencie">Referencie</a>
			</li>
			<li>
				<a href="/kontakt">Kontakt</a>
			</li>
		</ul>
	</div>
</footer>
