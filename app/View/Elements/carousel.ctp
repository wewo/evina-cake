<div class="container">
	<ul class="bxslider home">
		<?php for($slide_num = 1; $slide_num <= $carousel_data['count']; $slide_num++): ?>
			<li>
				<div class="slide-content" style="background-image: url('/img/carousel/<?= $carousel_data['folder']; ?>/<?= $slide_num; ?>.jpg');"></div>
			</li>
		<?php endfor; ?>
	</ul>
</div>
