<div class="wrapper" id="green-wrapper">
	<div class="container">
		<div class="row">
			<?= $this->element('menu'); ?>
		</div>

		<div class="row" id="carousel-holder">
			<?= $this->element('carousel'); ?>
		</div>
	</div>
</div>