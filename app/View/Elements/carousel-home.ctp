<div class="container">
	<ul class="bxslider home">
		<li>
			<div class="slide-content" id="home-slide-1">
				<p>Príďte sa pozrieť na nové<br><span class="big-green">darčekové predmety</span></p>
				<a href="/reklamne-predmety">Viac info</a>
			</div>
		</li>
		<li>
			<div class="slide-content" id="home-slide-2">
				<p>Oblečte si štýlový<br><span class="big-green">reklamný textil</span></p>
				<a href="/reklamny-textil">Viac info</a>
			</div>
		</li>
		<li>
			<div class="slide-content" id="home-slide-3">
				<p>Odvezte vašim obchodným partnerom<br><span class="big-green">kalendáre &amp; novoročenky</span></p>
				<a href="/kalendare">Viac info</a>
			</div>
		</li>
	</ul>
</div>