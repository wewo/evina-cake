<!DOCTYPE html>
<html>
<head>
	<?= $this->Html->charset(); ?>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>
		Evina | <?= $this->fetch('title'); ?>
	</title>
	<?= $this->element('favicon'); ?>
	<link href="//fonts.googleapis.com/css?family=Roboto:600,400,700,300italic,500italic&amp;subset=latin,latin-ext" rel="stylesheet" type="text/css">
	
	<?php

		echo $this->Html->css('bootstrap');
		echo $this->Html->css('jquery.bxslider');
		echo $this->Html->css('styles');
		echo $this->Html->css('carousel');


		echo $this->fetch('meta');
		echo $this->fetch('css');
		
	?>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body class="not-visible">

	<?php

	echo $this->element('green_wrapper');
	echo $this->fetch('content');
	echo $this->element('footer');
	?>

	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<?php
	echo $this->Html->script('jquery.bxslider.min');
	echo $this->Html->script('bootstrap.min');
	echo $this->Html->script('script');
	echo $this->fetch('script');
	?>

	<?php if($page === 'kontakt'): ?>
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCiWEZgdWEEaP6VWO0lLcrqOYEGDWzeFRE&amp;callback=mapInit" async defer></script>
	<?php endif; ?>

	
	
</body>
</html>
