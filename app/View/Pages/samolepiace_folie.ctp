<div class="wrapper" id="gray-wrapper">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<div class="h1-container">
					<h1>Samolepiace fólie</h1>
				</div>
			</div>
			<div class="col-xs-12 col-md-offset-2 col-md-8 gray-wrapper-text">
				<p>Polepiť obchod, výklad, prevádzku či auto firemným logom, alebo pútať pozornosť okoloidúcich reklamnou tabuľou, 3D nápisom, stojanom, svetelnou reklamou či bannerom, je už dnes úplne samozrejmé. Samolepiace fólie je možné vyrezať pomocou plotra alebo použiť digitálnu tlač.</p>

				<p>Pri výbere však treba dať pozor na to, ako dlho polep alebo potlač vydrží, aká bude jeho farebná stálosť a ďalšie dôležité veci.</p>
				<p><strong>Máme takmer 20-ročné skúsenosti v tejto práci, takže Vám odborne poradíme, vyrobíme, zrealizujeme&hellip;</strong></p>
			</div>
			<div class="col-xs-12">
				<hr>
			</div>
			<div class="col-xs-12 contact-us-single">
				<?= $this->element('contact_us'); ?>
			</div>
		</div>
	</div>
</div>
