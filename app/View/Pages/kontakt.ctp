<div class="wrapper" id="gray-wrapper">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<div class="h1-container">
					<h1>Kontakt</h1>
				</div>
			</div>
			<div class="col-xs-12 col-sm-offset-1 col-sm-3" id="contact-page-address">
				<strong>EVINA – REKLAMA, s.r.o.</strong><br><br>

				Piaristická 2<br>
				949 01  Nitra<br>
				Slovenská republika<br><br>

				<span style="margin-right: 12px">T/F</span>+421 (0) 37 / 654 23 11<br>
				<span style="margin-right: 26px">T</span>+421 (0) 37 / 654 23 01<br>
				<span style="margin-right: 22px">M</span>+421 (0) 905 323 982<br>
				<span style="margin-right: 30px">E</span><a href="#">evina@evina.sk</a>
			</div>

			<div class="col-xs-12 col-sm-offset-2 col-sm-6">
				<div id="success-message"></div>
				<div id="error-message"></div>
				<form action="/" method="POST" onsubmit="submitContactForm(); return false;" name="contact-form" id="contact-form">
					<div class="row">
						<div class="form-group col-xs-12">
							<input type="text" class="form-control" name="name" placeholder="Meno">
						</div>
						<div class="form-group col-xs-12">
							<input type="company" class="form-control" name="company" placeholder="Firma">
						</div>
						<div class="form-group col-xs-6 left">
							<input type="email" class="form-control" name="email" placeholder="E-mail">
						</div>
						<div class="form-group col-xs-6 right">
							<input type="phone" class="form-control" name="phone" placeholder="Mobil">
						</div>
						<div class="form-group col-xs-12">
							<textarea class="form-control" rows="5" name="message" placeholder="Text správy"></textarea>
						</div>
						<div class="form-group col-xs-12">
							<button type="submit" class="btn btn-default">Odoslať</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<div class="map-wrapper">
	<div class="container">
		<div class="row">
			<div class="hidden-xs col-sm-offset-1 col-sm-3" style="position:relative">
				<div id="drak"></div>
			</div>
		</div>
	</div>

	<div id="map" style="height: 450px"></div>
</div>
