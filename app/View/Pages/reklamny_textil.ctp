<div class="wrapper" id="gray-wrapper">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<div class="h1-container">
					<h1>Reklamný textil</h1>
				</div>
			</div>
			<div class="col-xs-12 col-md-offset-2 col-md-8 gray-wrapper-text">
				<p>Veľmi dobrou formou, ako zanechať v zákazníkovi naozaj dobrý dojem, je darovať kvalitný reklamný textil. Od tričiek, cez polokošele, bundy, mikiny, šiltovky, ale aj uteráky, župany a ďalšie. Textil s firemným brandingom, alebo vo firemných farbách, je vhodný aj vtedy, ak chcete, aby boli Vaši zamestnanci jednotne oblečení. Zákazník sa v takejto predajni ľahšie orientuje, a pokiaľ Vaši zamestnanci zákazníkov navštevujú, samozrejme Vám robia reklamu všade.</p>
				<p>Ponúkame Vám len textil vysokej kvality, na ktorý je možné umiestniť Vašu reklamu.</p>
				<p><strong>Pozrite si, prosím, náš katalóg a kontaktujte nás.<br>Niektoré tričká a ďalší textil si môžete pozrieť a vybrať aj priamo v našej vzorkovni. Neváhajte a príďte!</strong></p>
			</div>
			<div class="col-xs-12">
				<hr>
			</div>
			<div class="col-xs-12">
				<div class="row">
					<div class="col-xs-12 col-md-7" id="catalogs-holder">
						<h2>Prelistujte si katalóg</h2>
						<p>Kliknite a pozrite si ho on-line</p>
						<div class="row">
							<div class="col-xs-12">
								<a href="http://www.reklamnytextil.sk/" class="catalog-link tall" id="catalog-link-evina-textil-catalog-2016">
									<div class="catalog-hover-corner"></div>
								</a>
							</div>
						</div>
					</div>
					<?= $this->element('contact_us'); ?>
				</div>
			</div>
		</div>
	</div>
</div>
