<div class="wrapper" id="gray-wrapper">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<div class="h1-container">
					<h1>Reklamné predmety</h1>
				</div>
			</div>
			<div class="col-xs-12 col-md-offset-2 col-md-8 gray-wrapper-text">
				<p>Reklamné predmety s Vašou značkou sú skvelá vec! Určite ste už mnohé dali, a mnohé aj dostali, viac či menej vydarené. Reklamný predmet musí zaujať, potešiť, možno pobaviť, byť užitočný, aby ho obdarovaný používal rád a často, a vždy si tým pripomenul Vás.</p>
				<p>Vieme, ako vybrať najvhodnejšie reklamné predmety pre Vašich zákazníkov, obchodných partnerov, či zamestnancov. Poradíme najlepšiu metódu brandingu, vyrobíme, zabalíme, dodáme.</p>
				<p><strong>Pozrite si, prosím, náš katalóg a kontaktujte nás.<br>Niektoré predmety si môžete vybrať a pozrieť priamo v našej vzorkovni, neváhajte a príďte.</strong></p>
			</div>
			<div class="col-xs-12">
				<hr>
			</div>
			<div class="col-xs-12">
				<div class="row">
					<div class="col-xs-12 col-md-7" id="catalogs-holder">
						<h2>Prelistujte si katalóg</h2>
						<p>Kliknite a pozrite si ho on-line</p>
						<div class="row">
							<div class="col-xs-6">
								<a href="http://www.excursion.sk/" class="catalog-link" id="catalog-link-evina-predmety-catalog-2016">
									<div class="catalog-hover-corner"></div>
								</a>
							</div>
							<div class="col-xs-6">
								<a href="http://www.presentpremium.com/" class="catalog-link" id="catalog-link-predmety-present-2015">
									<div class="catalog-hover-corner"></div>
								</a>
							</div>
						</div>
					</div>
					<?= $this->element('contact_us'); ?>
				</div>
			</div>
		</div>
	</div>
</div>
