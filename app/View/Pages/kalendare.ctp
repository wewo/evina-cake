<div class="wrapper" id="gray-wrapper">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<div class="h1-container">
					<h1>Kalendáre, novoročenky</h1>
				</div>
			</div>
			<div class="col-xs-12 col-md-offset-2 col-md-8 gray-wrapper-text">
				<p>Viete, do čoho sa pozeráme každý deň? Áno, aj do peňaženky, ale teraz máme na mysli kalendár, prípadne diár. Kalendár alebo diár s Vašou reklamou je ideálny darček pred koncom roka, ktorý nielen poteší, ale urobí aj svoju "reklamnú prácu" počas celého roka. Pripomína Vašu firmu 365 dní v roku za nepomerne nižšie náklady ako akýkoľvek iný druh reklamy. Vyberte si z nášho katalógu a darujte Vašim obchodným partnerom originálny kalendár!</p>
				<p>Novoročenka je v prvom rade prejavom úcty voči obchodným partnerom, či zamestnancom. Zaslaním novoročenky vyjadrujete poďakovanie a dobrý vzťah a stalo sa veľmi peknou, a medzi firmami stále očakávanou, tradíciou.</p>
				<p><strong>Pozrite si, prosím, náš katalóg a kontaktujte nás.</strong></p>
			</div>
			<div class="col-xs-12">
				<hr>
			</div>
			<div class="col-xs-12">
				<div class="row">
					<div class="col-xs-12 col-md-7" id="catalogs-holder">
						<h2>Prelistujte si katalóg</h2>
						<p>Kliknite a pozrite si ho on-line</p>
						<div class="row">
							<div class="col-xs-6">
								<a href="http://www.kalendar365.eu/" class="catalog-link tall" id="catalog-link-evina-kalendare-catalog-2016">
									<div class="catalog-hover-corner"></div>
								</a>
							</div>
							<div class="col-xs-6">
								<a href="http://www.kalendare-sk.sk/" class="catalog-link tall" id="catalog-link-evina-kalendare_2-catalog-2016">
									<div class="catalog-hover-corner"></div>
								</a>
							</div>
						</div>
					</div>
					<?= $this->element('contact_us'); ?>
				</div>
			</div>
		</div>
	</div>
</div>
