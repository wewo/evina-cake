<div class="wrapper" id="gray-wrapper">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<div class="h1-container">
					<h1>Referencie</h1>
				</div>
			</div>
			<div class="col-xs-12 gray-wrapper-text">
				<div class="row">
					<?php foreach ($data['logos'] as $i => $logo): ?>
						<div class="col-sm-3 col-xs-6">
							<div class="logo-holder">
								<img src="<?= 'img/logos/'.$logo; ?>" alt="<?= $logo; ?>" />
							</div>
						</div>
						<?php if(($i+1)%4 == 0): ?>.
							</div>
							<div class="row">
						<?php endif; ?>
					<?php endforeach; ?>
				</div>
			</div>
			<div class="col-xs-12">
				<hr>
			</div>
			<div class="col-xs-12 contact-us-single">
				<?= $this->element('contact_us'); ?>
			</div>
		</div>
	</div>
</div>
