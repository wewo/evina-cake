<div class="wrapper" id="gray-wrapper">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<div class="h1-container">
					<h1>Firemné tlačoviny</h1>
				</div>
			</div>
			<div class="col-xs-12 col-md-offset-2 col-md-8 gray-wrapper-text">
				<p>Vizitky, hlavičkový papier, obálky, tlačivá, zmluvy, rôzne poznámkové bloky, katalógy, letáky, plagáty, samolepky a mnohé ďalšie užitočné tlačoviny, bez ktorých sa firma nezaobíde.</p>

				<p>Okrem toho, že jednotný firemný dizajn vyjadruje vysokú úroveň Vašej firemnej kultúry, je to aj miesto pre Vašu reklamu.</p>

				<p><strong>Tak prečo ho nevyužiť naplno?</strong></p>
			</div>
			<div class="col-xs-12">
				<hr>
			</div>
			<div class="col-xs-12 contact-us-single">
				<?= $this->element('contact_us'); ?>
			</div>
		</div>
	</div>
</div>