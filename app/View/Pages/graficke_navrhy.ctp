<div class="wrapper" id="gray-wrapper">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<div class="h1-container">
					<h1>Grafické návrhy</h1>
				</div>
			</div>
			<div class="col-xs-12 col-md-offset-2 col-md-8 gray-wrapper-text">
				<p><strong>Nechajte si vytvoriť grafické návrhy u nás.</strong></p>

				<p>Pracujeme so skúsenými, kreatívnymi grafikmi, ktorí svoju prácu robia nielen radi, ale aj dobre. Dobrý grafik vie dať Vašu predstavu na papier, pretože chápe, že vy ste jej tvorcom. Dobrý grafik vie urobiť niekoľko návrhov, ak predstavu nemáte.</p>

				<p>Vyberie správne farby, správne písmo, fotku, obrázok, text, všetko vhodne umiestni tak, aby bola Vaša grafika zaujímavá a zapamätateľná. Využite služby našich grafikov a Vaša myšlienka si nájde cestu k Vašim zákazníkom!</p>
			</div>
			<div class="col-xs-12">
				<hr>
			</div>
			<div class="col-xs-12 contact-us-single">
				<?= $this->element('contact_us'); ?>
			</div>
		</div>
	</div>
</div>