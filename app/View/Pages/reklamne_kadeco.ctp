<div class="wrapper" id="gray-wrapper">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<div class="h1-container">
					<h1>Reklamné kadečo</h1>
				</div>
			</div>
			<div class="col-xs-12 col-md-offset-2 col-md-8 gray-wrapper-text">
				<p>Zaujalo Vás reklamné kadečo? Tieto reklamné predmety sa síce nehodia do žiadnej bežnej kategórie, ale stále sú veľmi užitočné, pekné a nápadité. Účelom je urobiť pre Vašich zákazníkov, obchodných partnerov, či zamestnancov niečo navyše, niečo, čo poteší, zaujme, vyjadrí Vašu vďaku.</p>
				<p>Najviac žiadané sú papierové a igelitové reklamné tašky, lepiace pásky s logom firmy na označenie tovaru, či zásielok, šanóny, obaly, púzdra, vlajky, či už stolové alebo aj stožiarové, ale aj spoločenské hry a rôzne hračky. Fantázii sa však medze nekladú.</p>
				<p><strong>Reklamným predmetom teda môže byť čokoľvek, čo vystihuje Vašu prácu, vhodne označené Vašim firemným logom či značkou.</strong></p>
			</div>
			<div class="col-xs-12">
				<hr>
			</div>
			<div class="col-xs-12 contact-us-single">
				<?= $this->element('contact_us'); ?>
			</div>
		</div>
	</div>
</div>
