<div class="wrapper" id="gray-wrapper">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<div class="h1-container">
					<h1>O nás</h1>
				</div>
			</div>
			<div class="col-xs-12 col-md-6 gray-wrapper-text">
				<p>Keď sme v minulom storočí, v ére socializmu, hrdo nosili vzácne igelitové tašky s nápisom Neckermann, a pripadali sme si ako „praví západniari“, ani nám nenapadlo, že je to obyčajná reklama. Dnes je reklamný predmet úplne bežná vec. Aby sme reklamou zaujali, musíme vymýšľať novinky, prekvapiť, známe veci ukázať v novom svetle, spraviť pre zákazníka nielen maximum, ale ešte o kúsok viac. A presne to je naša filozofia.</p>

				<p>Sme na reklamnom trhu od roku 1997, teda prakticky od jeho vzniku na Slovensku … a stále nás to baví! Za tie roky skúseností s mnohými klientmi a neustále sa meniacimi požiadavkami trhu, sme prišli na to, že len <strong>osobný prístup, vysoká profesionalita a bezchybná kvalita</strong> prinášajú úspech. Rovnako ako vo Vašom biznise.</p>
			</div>
			<div class="col-xs-12 col-md-6 gray-wrapper-text">
				<p>Našou hlavnou oblasťou sú reklamné predmety a reklamný textil s brandingom, ako nazývame označenie Vašim logom a pod. Ponúkame tiež firemné kalendáre, diáre, novoročenky, tašky, vlajky a ďalšie, navrhneme pre Vás firemné tlačoviny, urobíme polep Vášho auta, výkladu, vyrobíme reklamné tabule, či stojany. Spolupracujeme s kvalitnými grafikmi, takže Vám veľmi radi pomôžeme aj s grafickými návrhmi.</p>

				<p>V reklame je možné (takmer) všetko, preto ak chcete to najlepšie, ste na správnom mieste! Nejde nám o najlacnejšiu ponuku, chceme Vám dať <strong>najlepšiu ponuku</strong>. Príďte si pozrieť darčekové predmety priamo k nám, alebo si prelistujte naše katalógy a určite si vyberiete. Dajte nám vedieť, aké sú Vaše požiadavky a my sa postaráme o to, aby ste boli spokojní.</p>
			</div>
		</div>
	</div>
</div>

<div class="wrapper" id="circle-links-holder">
	<div class="container">
		<div class="row">
			<div class="col-xs-6 col-md-3 circle-link-container">
				<a class="circle-link reklamne-predmety"	href="/reklamne-predmety">Reklamné predmety<br><span>viac info</span></a>
			</div>
			<div class="col-xs-6 col-md-3 circle-link-container">
				<a class="circle-link reklamny-textil"		href="/reklamne-predmety">Reklamný textil<br><span>viac info</span></a>
			</div>
			<div class="col-xs-6 col-md-3 circle-link-container">
				<a class="circle-link kalendare"			href="/kalendare">Kalendáre, novoročenky<br><span>viac info</span></a>
			</div>
			<div class="col-xs-6 col-md-3 circle-link-container">
				<a class="circle-link samolepiace-folie"	href="/samolepiace-folie">Samolepiace fólie<br><span>viac info</span></a>
			</div>
			<div class="col-xs-6 col-md-3 circle-link-container">
				<a class="circle-link firemne-tlacoviny"	href="/firemne-tlacoviny">Firemné tlačoviny<br><span>viac info</span></a>
			</div>
			<div class="col-xs-6 col-md-3 circle-link-container">
				<a class="circle-link reklamne-kadeco"		href="/reklamne-kadeco">Reklamné kadečo<br><span>viac info</span></a>
			</div>
			<div class="col-xs-6 col-md-3 circle-link-container">
				<a class="circle-link graficke-navrhy"		href="/graficke-navrhy">Grafické návrhy<br><span>viac info</span></a>
			</div>
			<div class="col-xs-6 col-md-3 circle-link-container">
				<a class="circle-link referencie"			href="/referencie">Referencie<br><span>viac info</span></a>
			</div>
		</div>
	</div>
</div>
